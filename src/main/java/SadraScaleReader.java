import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;


public class SadraScaleReader {
    private String readDataFrom = Consts.COM_PORT_ADDRESS;
    private SerialPort comPort;
    private static String result = "";

    public SadraScaleReader() {
        start();
    }

    private void start() {
        if (comPort == null) {
            comPort = SerialPort.getCommPort(readDataFrom);
        }
        if (!comPort.isOpen()) {
            System.out.println("start connecting to " + readDataFrom);
            comPort.openPort();
            comPort.setBaudRate(9600);
            comPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
            comPort.setNumDataBits(8);
            comPort.setNumStopBits(1);
            comPort.setParity(SerialPort.NO_PARITY);

            final class PacketListener implements SerialPortPacketListener {
                @Override
                public int getListeningEvents() {
                    return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
                }

                @Override
                public int getPacketSize() {
                    return 18;
                }

                @Override
                public void serialEvent(SerialPortEvent event) {
                    byte[] newData = event.getReceivedData();
                    System.out.println("Received data of size: " + newData.length);
                    result = "";
                    for (int i = 0; i < newData.length; ++i) {
                        System.out.print((char) newData[i]);
                        result += (char) newData[i];
                    }
                    System.out.println("\n");
                }
            }

            PacketListener packetListener = new PacketListener();
            comPort.addDataListener(packetListener);

        } else
            System.out.println("comport was already open!");
    }

    public String read() throws Exception {
        if (comPort == null) {
            throw new Exception("comPort is null");

        } else if (!comPort.isOpen()) {
            throw new Exception("comPort isn't open.");
        } else {
            comPort.writeBytes(new byte[]{'*'}, 1);

            System.out.println("star has written!");

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return result.substring(7, 7 + 5);
//            while (true) {
//                String result1 = result;
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                String result2 = result;
//                if (result1.equals(result2) && result.length() == 5) {
//                    System.out.println("final result:" + result1);
//                    return result1;
//                }
//            }
        }
    }

}
