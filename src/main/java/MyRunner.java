public class MyRunner {

    SadraScaleReader sadraScaleReader;

    public MyRunner() {
        sadraScaleReader = new SadraScaleReader();
    }

    public String runAll(String productCode, String title, String invoiceId) throws Exception {
        String myConst = "2";
        System.out.println("start reading ...");
        String weight = sadraScaleReader.read();
        System.out.println("weight has received:" + weight);
        System.out.println("start generating barcode ...");
        MyBarcodeGenerator.run(myConst, productCode, weight);
        System.out.println("finish generating barcode!");
        System.out.println("start generating label ...");
        String weightInKilo = weight.substring(0, 2) + "." + weight.substring(2);
        PngManager.run(title, weightInKilo, invoiceId);
        System.out.println("finish generating label!");
        System.out.println("start printing ...");
        Printing.run();
        System.out.println("finish printing!");
        return weight;
    }

}
