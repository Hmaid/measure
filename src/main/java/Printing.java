import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.File;
import java.io.IOException;

public class Printing {

    static String path = Consts.DESTINATION_DRIVER + "result.png";

    public static void run() {
        PrinterJob pj = PrinterJob.getPrinterJob();
        PageFormat format = pj.getPageFormat(null);
        Paper paper = format.getPaper();
        paper.setImageableArea(0.0, 0.0, format.getPaper().getWidth(), format.getPaper().getHeight());
        format.setPaper(paper);

        pj.setPrintable(new Printable()
        {
            @Override
            public int print(Graphics pg, PageFormat pf, int pageNum) {
                if (pageNum > 0)
                    return Printable.NO_SUCH_PAGE;

                Graphics2D g2 = (Graphics2D)pg;
                g2.translate(pf.getImageableX(), pf.getImageableY());
                BufferedImage originalImage;
                try {
                    originalImage = ImageIO.read(new File(path));
                    g2.drawImage(originalImage, 15, 5, 150, 91, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return Printable.PAGE_EXISTS;
            }
        }, format);


        try {
            pj.print();
        } catch (PrinterException e) {
            e.printStackTrace();
        }
    }


    static public void main(String[] args) {

    }
}
