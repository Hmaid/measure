import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.ColorConvertOp;
import java.awt.image.DataBuffer;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.IOException;



import java.awt.image.BufferedImage;

public class PngManager {
    static String title = "گوشت گوساله منجمد برزیلی تنظیم بازار";
    static int splitCharSize = 23;
    static int totalWidth = 6500;
    static int totalHeight = 3953;
    static int titleFontSize = 550;
    static int weightFontSize = 450;
    static int invoiceFontSize = 1000;
    static int yOffset = 10;
    static int xOffset = 50;
    static String firstLine = titleSplitter(title)[0];
    static String secondLine = titleSplitter(title)[1];
    static String filePath = Consts.DESTINATION_DRIVER;
    static String weight = "0.34";
    static String weightText = "";
    static String kiloText = " کیلوگرم ";
    static String invoiceId = "1234";
    static String weightLine = weightText + weight + kiloText;
    static String barcodeFileName = "code14.png";
    static String outputFileName = "result.png";

    public static void run(String title, String weight, String invoiceId) {
        setTitle(title);
        setWeight(weight);
        setInvoiceId(invoiceId);
        weightLine = weightText + weight + kiloText;
        String p1 = createPngFromStr(firstLine, filePath, titleFontSize, "firstLine");
        String p2 = createPngFromStr(secondLine, filePath, titleFontSize,"secondLine");
        String p3 = createPngFromStr(weightLine, filePath, weightFontSize,"weightLine");
        String p4 = createPngFromStr(invoiceId, filePath, invoiceFontSize ,"invoiceId");

        concatenatePNGs(filePath, p1, p2, p3, p4, barcodeFileName, outputFileName, totalWidth, totalHeight);
    }

    private static void setTitle(String title) {
        PngManager.title = title;
        PngManager.firstLine = titleSplitter(title)[0];
        PngManager.secondLine = titleSplitter(title)[1];
    }

    private static void setWeight(String weight) {
        PngManager.weight = weight;
    }

    private static void setInvoiceId(String invoiceId) {
        PngManager.invoiceId = invoiceId;
    }

    private static String[] titleSplitter(String str) {
        String[] myString = new String[2];
        if (str.length() > splitCharSize) {
            int splitIndex = 0;
            for (int i = splitCharSize; i > 0; i--) {
                if (str.charAt(i) == ' ') {
                    splitIndex = i;
                    break;
                }
            }
            myString[0] = str.substring(0, splitIndex);
            if (str.substring(splitIndex).length() > splitCharSize) {
                myString[1] = str.substring(splitIndex + 1, 2 * splitCharSize - 3) + "...";
            } else {
                myString[1] = str.substring(splitIndex + 1);
            }
        } else {
            myString[0] = str;
            myString[1] = " ";
        }
        return myString;
    }

    public static void main(String[] args) {
//        String[] a = titleSplitter("hamid fazli khojir is here to help you!");
//        System.out.println(a[0]);
//        System.out.println(a[1]);
        run("گوشت منجمد برزیلی تنظیم بازار", "1235", "4568");
//        newManager();
    }

    private static String createPngFromStr(String strToPng, String filePath, Integer fontSize, String outputFileName) {
        try {
            BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = img.createGraphics();
            Font font = new Font("Arial", Font.PLAIN, fontSize);//48//Courier//Arial
            g2d.setFont(font);
            FontMetrics fm = g2d.getFontMetrics();
            int width = fm.stringWidth(strToPng);
            int height = fm.getHeight();
            g2d.dispose();

            img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            g2d = img.createGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
            g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            g2d.setFont(font);
            fm = g2d.getFontMetrics();
            g2d.setColor(Color.BLACK);
            g2d.drawString(strToPng, 0, fm.getAscent());
            g2d.dispose();

            ImageIO.write(img, "png", new File(filePath, outputFileName));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return (outputFileName);
    }

    private static void concatenatePNGs(String path, String firstLineName, String secondLineName,
                                       String weightName, String invoiceIdName, String barcodeName, String outputName,
                                       int totalWidth, int totalHeight) {

        try {
            BufferedImage subTitle = null;
            BufferedImage title = ImageIO.read(new File(path, firstLineName));
            if(secondLineName != null) {
                subTitle = ImageIO.read(new File(path, secondLineName));
            }
            BufferedImage weight = ImageIO.read(new File(path, weightName));
            BufferedImage invoiceId = ImageIO.read(new File(path, invoiceIdName));
            BufferedImage barCode = ImageIO.read(new File(path, barcodeName));

            BufferedImage combined = new BufferedImage(totalWidth, totalHeight, BufferedImage.TYPE_BYTE_BINARY);


            Graphics g = combined.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, totalWidth, totalHeight);
            g.drawImage(title, (totalWidth - title.getWidth()) / 2, yOffset, null);
            if(secondLineName != null) {
                g.drawImage(subTitle, (totalWidth - subTitle.getWidth()) / 2, yOffset + title.getHeight(), null);
            }
            g.drawImage(weight, (totalWidth - weight.getWidth() - xOffset)  , yOffset + title.getHeight() + subTitle.getHeight(), null);
            g.drawImage(invoiceId, (xOffset * 3), yOffset + title.getHeight() + subTitle.getHeight(), null);
            g.drawImage(barCode, 3 * xOffset + (totalWidth - barCode.getWidth()) / 2 , 20 + totalHeight - barCode.getHeight() - yOffset, null);

            ImageIO.write(combined, "png", new File(path, outputName));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }


}

