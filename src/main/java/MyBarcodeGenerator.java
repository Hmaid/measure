
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.configuration.DefaultConfiguration;
import org.krysalis.barcode4j.*;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class MyBarcodeGenerator {

    static String myConst = "2";
    static String productCode = "123456";
    static String weight = "12345";
    static String textToBarcode = myConst + productCode + weight;
    static String path = Consts.DESTINATION_DRIVER + "code14.png";

    private static void setMyConst(String myConst) {
        MyBarcodeGenerator.myConst = myConst;
    }

    private static void setProductCode(String productCode) {
        MyBarcodeGenerator.productCode = productCode;
    }

    private static void setWeight(String weight) {
        MyBarcodeGenerator.weight = weight;
    }

    public static void setTextToBarcode() {
        MyBarcodeGenerator.textToBarcode = myConst + productCode + weight;
    }

    public static void run(String myConst, String productCode, String weight) {
        System.out.println("start generating...");
        setMyConst(myConst);
        setProductCode(productCode);
        setWeight(weight);
        setTextToBarcode();
        generate();
        System.out.println("barcode has generated!");
    }

    private static void generate() {
        BarcodeUtil util = BarcodeUtil.getInstance();
        org.krysalis.barcode4j.BarcodeGenerator gen = null;
        try {
            gen = util.createBarcodeGenerator(buildCfg("ean-13"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (BarcodeException e) {
            e.printStackTrace();
        }

        OutputStream fout = null;
        try {
            fout = new FileOutputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int resolution = 4300;
        BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                fout, "image/png", resolution, BufferedImage.TYPE_BYTE_BINARY, true, 0);
        gen.generateBarcode(canvas, textToBarcode);
        try {
            canvas.finish();
            BufferedImage originalImage = ImageIO.read(new File(path));
            BufferedImage resized = originalImage.getSubimage(0, originalImage.getHeight() / 2, originalImage.getWidth(), originalImage.getHeight() / 2);
            ImageIO.write(resized, "png", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Configuration buildCfg(String type) {
        DefaultConfiguration cfg = new DefaultConfiguration("barcode");

        //Bar code type
        DefaultConfiguration child = new DefaultConfiguration(type);
        cfg.addChild(child);

        //Human readable text position
        DefaultConfiguration attr = new DefaultConfiguration("human-readable");
        DefaultConfiguration subAttr = new DefaultConfiguration("placement");
        subAttr.setValue("bottom");
        attr.addChild(subAttr);

        child.addChild(attr);
        return cfg;
    }

    public static void main(String[] args) {
        generate();
    }


}
